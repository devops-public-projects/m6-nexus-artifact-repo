# Artifact Repository Manager with Nexus
We will create a remote server using a DigitalOcean droplet and install Nexus Artifact Repo Manager then we'll upload our locally built artifacts and configure Nexus policies.

# Technologies Used
- DigitalOcean Droplets
- Linux (Ubuntu)
- Sonatype Nexus Artifact Repository Manager
- Java
- Gradle
- Maven

# Project Description
- Setup and configure a server on DigitalOcean
- Install and configure Nexus Artifact Repository Manager on remote server
- Create a new user on Nexus with relevant permissions
- Java Gradle Project: Build Jar & Upload to Nexus
- Java Maven Project: Build Jar & Upload to Nexus
- Configure Nexus policies for cleanup

# Prerequisites
- Local machine (I will be using Ubuntu, any OS is fine but steps may vary depending on tools/commands used)
	- Your machine should have Git, Java, and Gradle installed. If not, you can use `apt install` for all three.
		- Java version on local machine is java-17-openjdk-amd64
- DigitalOcean account and ability to create a droplet
	- Configure it with your personal SSH key from your local machine

# Steps for Setup
## Configure DigitalOcean
### Create Droplet
1. Create an account if you don't already have one
2. Create a new project
3. Create a new Droplet
	- Location = Closest to you, we'll use NY
	- Ubuntu 22.10 x64
	- Basic Plan
	- Regular (SSD) / $48/month
		- 8GB RAM, 160GB Storage, 5TB Transfer (Higher memory is required for Nexus to function properly)
	- Authentication > SSH
		- Add your **public** SSH key from your local machine here
			- cat ~/.ssh/id_rsa.pub on Linux
			- If you don't have a key you can use the `ssh-keygen` command on Linux
4.  Finish by pressing **Create Droplet**

![Droplet Created](/images/m6-1-droplet-created.png)

### Configure Firewall
1. DigitalOcean Menu > Droplets > Networking > Firewalls > Edit > Create Firewall
	- Set a name for the firewall that is descriptive
	- Set inbound SSH (port 22) ability for your IP address
		- You can get your IP by going to [What's my IP?](https://www.whatismyip.com/) or alternative methods
	- **Create Firewall**
2. Go to Networking > Firewall > Droplets > Add Droplet
	- Add your new Droplet to this firewall for it to take effect

## Install Dependencies and Nexus
### Local Machine
1. `ssh root@DROPLET_IP`
	- Get the IP from the DigitalOcean droplet menu and copy it

### Remote Server
1. Install Java
	- `apt install openjdk-8-jre-headless`
		- This version is required by Nexus
2. Install Net Tools
	- `apt install net-tools`
3. Install & Configure Nexus
	- `cd /opt`
		- We'll install the package here
	- Get download link from [Sonatype Nexus Download Page](https://help.sonatype.com/repomanager3/product-information/download)
	- `wget https://download.sonatype.com/nexus/3/nexus-3.57.0-01-unix.tar.gz`
		- Download file to /opt
	- `tar -zxvf nexus-3.57.0-01-unix.tar.gz`
		- Unpack the files
4. Add Service Account for Nexus to run as
	- `adduser nexus`
		- Set password and name fields, etc
5. Change Nexus folder permissions from root to nexus user
	- `chown -R nexus:nexus nexus-3.53.0-01`
	- `chown -R nexus:nexus sonatype-work`
		- Recursively change both directories, allows running the binaries in nexus folder, and read/write to sonatype-work

![Change Folder Ownerships](/images/m6-2-change-folder-ownerships.png)

6. Set Nexus configuration to run as nexus user
	- `vim nexus-3.53.0-01/bin/nexus.rc`
	- Modify the line to uncomment and add nexus as the user
		- `run_as_user="nexus"`
7. Run Nexus as nexus user
	- `su nexus`
	- `/opt/nexus-3.57.0-01/bin/nexus start`

8. Verify Nexus is running and listening
 	- `ps aux | grep nexus`
		- Process 3073 is running for nexus
	- `netstat -lnpt`
		- Shows nexus is listening for external requests on port 8081

![Nexus is running](/images/m6-3-nexus-is-running.png)

![Nexus is listening](/images/m6-4-nexus-is-listening.png)

8. Modify Droplet firewall for Nexus access
	- Add an exception to your DigitalOcean firewall to allow your IP to access port 8081 for inbound connections.

9. Verify website is accessible
- Navigate in your browser to DROPLET_IP:8081
	- If everything works you will be greeted with the repository website

![Nexus is accessible via the web](/images/m6-5-nexus-accepting-requests.png)

## Complete Initial Nexus Setup
### Web Browser
1. Navigate to DROPLET_IP:8081
2. Log in with admin user and password
	- password is available in the `/opt/sonatype-work/nexus3/admin.password` file
3. Setup Wizard
	- Change the admin password
	- Enable Anonymous Mode
	- `Finish`

## Publish Artifact to Nexus Repository
### Create a Nexus user and grant is upload permissions
1. From the Nexus UI in the browser go to Settings > Security > Users
2. Create local user
	- Set the username, first/last name, email, password
	- Set to active
	- Add to a role (nx-anonymous for now)
	- `Create local user`
3. Create a new role
	- Security > Roles > Create Role
	- Type: Nexus Role
	- Set role id, name (ex: nx-java)
	- Set permissions
		- We'll use the maven-snapshots repository permissions
		- Giving least permissions necessary, we'll use the `view-*` permissions for this role
	- `Save`
4. Assign Role to the user
	- Security > Users
	- Modify newly made user
	- Remove nx-anonymous role and add nx-java role
	- `Save`

![Nexus role applied to user](/images/m6-6-new-nexus-role-applied.png)

### Gradle Project | Configure with Nexus
1. Download sample project
	- `mkdir ~/gradle-java-app`
	- `cd ~/gradle-java-app`
	- `git clone https://gitlab.com/twn-devops-bootcamp/latest/06-nexus/java-app.git`
2. Modify project build.gradle to connect to our Nexus server
	- Edit `build.gradle` file to include a publishing block
	- Add a `gradle.properties` file to add username and password fields to prevent hard coding it into repository code.

![Configuring Gradle](/images/m6-7-configuring-gradle-for-nexus.png)

![Gradle.properties Username and Password](/images/m6-8-nexus-account-details.png)

3. Set our App name when it gets built
- In `settings.gradle` add a line for our app name: `rootProject.name = 'my-app'` to customize it

4. Build Project
	- `gradle build`
		- From the terminal in the project folder or in your editor with the project open
		- The build folder is created and the libs folder holds our JAR file
	- `cd ~/gradle-java-app`
	- `gradle publish`
		- We configured this by adding the `maven-publish plugin` in to the `build.gradle` file
	- We can now see the snapshot is stored in our repository

![Our JAR is now in maven-releases Nexus repository](/images/m6-9-snapshot-in-repository.png)

### Maven Project | Configure with Nexus
1. Download sample project
	- `cd ~`
	- `git clone https://gitlab.com/twn-devops-bootcamp/latest/06-nexus/java-maven-app.git`
	- `cd java-maven-app`
2. Use an editor to open the projects `pom.xml` file to configure Nexus
	- First we add in the plugin to use within the `<build>` tag
	- Then we add a line to configure Nexus with the `<distributionManagement>` tag

![Code to add Maven deploy plugin](/images/m6-10-add-in-maven-deploy-plugin.png)

![Code to add Nexus configuration to Maven](/images/m6-11-add-in-maven-nexus-configuration.png)

3. Configure user credentials for Maven
	- `cd ~/.m2`
	- `vim settings.xml`
		- Add in the code below with your username and password that can be used by Maven for the project

![Maven Global Settings](/images/m6-12-maven-global-settings-credentials.png)

4. Build the JAR file
	-  `cd java-maven-app`
	- `mvn package`
	- `ls target`
		- Will show us our built JAR file
5. Deploy the JAR file
	- `mvn deploy`

![Maven JAR Upload to Nexus Success](/images/m6-13-maven-artifact-upload-success.png)
